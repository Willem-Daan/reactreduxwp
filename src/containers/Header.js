import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';
import { fetchSiteTitle } from '../actions';

import Posts from './Posts';
import Title from './Pagetitle';

//styles
import logo from '../logo.svg';

class Header extends React.Component {
  render(){
    return(
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Title />
      </header>
    )
  }
}

export default Header;
