import React, { Component } from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

//redux
import {connect} from 'react-redux';
import {fetchSiteTitle} from '../actions';


class Title extends React.Component {

  componentWillMount() {
    this.props.fetchSiteTitle();
  }

  render() {
    return (
      <div className="header-container">
        <h1>{ReactHtmlParser(this.props.pagetitle.pagetitle)}</h1>
        <h2>{ReactHtmlParser(this.props.pagetitle.desc)}</h2>
      </div>
    );
  }
}

function mapStateToProps({pagetitle,desc}){
  return {pagetitle, desc}
}

const mapDispatchToProps = {
  fetchSiteTitle
}

export default connect(mapStateToProps, mapDispatchToProps)(Title);
