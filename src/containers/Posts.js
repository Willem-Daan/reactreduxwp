import React, { Component } from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import { Link } from 'react-router-dom';


//redux
import {connect} from 'react-redux';
import {fetchPosts} from '../actions';


class Posts extends React.Component {

  componentWillMount() {
    this.props.fetchPosts();
  }

  getPosts(){
    const postList = this.props.posts.posts;
    const posts = postList.map((post, index)=>{

        return(
          // to={`blog/${post.slug}`}
          <li key={index}><Link to={{pathname: `blog/${post.slug}`, state: { transition: 'slideLeft' }}}>{ ReactHtmlParser( post.title.rendered ) }></Link></li>
        )
    });
    return posts
  }

  render() {
    return (
      <div>
        <ul>{this.getPosts()}</ul>
      </div>
    );
  }
}

function mapStateToProps({posts}){
  return {posts}
}

const mapDispatchToProps = {
  fetchPosts
}

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
