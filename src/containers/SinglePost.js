import React, { Component } from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import { Link } from 'react-router-dom';

//redux
import {connect} from 'react-redux';
import {fetchSinglePost} from '../actions';

import Header from './Header';

class SinglePost extends React.Component {

  componentWillMount() {
    let slug = this.props.match.params.slug;
    // let slug = url.substring(1);
    this.props.fetchSinglePost('posts',slug);
  }

  getPosts(){
    const postList = this.props.posts.posts;
    const posts = postList.map((post, index)=>{
        return(
          <div key={index}  className="App single">
            <Header />
            <Link to={{pathname: '/', state: { transition: 'slideRight' }}}>Terug</Link>
            <div className="entry-post">
              <h2>{post.title.rendered}</h2>
              {ReactHtmlParser(post.content.rendered)}
            </div>
          </div>
        )
    });
    return posts
  }

  render() {
    return (
      <div>
        {this.getPosts()}
      </div>
    );
  }
}

function mapStateToProps({posts}){
  return {posts}
}

const mapDispatchToProps = {
  fetchSinglePost
}

export default connect(mapStateToProps, mapDispatchToProps)(SinglePost);
