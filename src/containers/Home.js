import React, { Component } from 'react';

//redux
import {connect} from 'react-redux';
import {fetchSiteTitle} from '../actions';

import Posts from './Posts';
import Header from './Header';

class Home extends React.Component {

  render() {
    return (
      <div className="App">
        <Header />
        <Posts />
      </div>
    )
  }

}

export default Home;
