//react
import React, { Component } from 'react';
import { Router, Route, Switch, Link} from 'react-router-dom'
import { routerMiddleware, connectRouter, ConnectedRouter} from 'connected-react-router';
// import { TransitionGroup, CSSTransition } from "react-transition-group";
import { CSSTransitionGroup } from 'react-transition-group' // ES6


import createHistory from 'history/createBrowserHistory';

//Redux
import { Provider } from 'react-redux';
import store from './store';

import './App.css';

import Home from './containers/Home';
import SinglePost from './containers/SinglePost';

const history = createHistory();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Route render={({ location }) => (
              <CSSTransitionGroup
                transitionName={ location.state ? location.state.transition : "fade" }
                transitionEnterTimeout={500}
                transitionLeaveTimeout={300}>
                <Switch key={location.key} location={location} name={location.state ? location.state.name : false}>
                  <Route exact path="/" component={Home}/>
                  <Route exact path="/blog" component={Home}/>
                  <Route exact path="/blog/:slug" component={SinglePost}/>
                </Switch>
              </CSSTransitionGroup>
          )}/>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
