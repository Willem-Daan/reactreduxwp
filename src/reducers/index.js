import {combineReducers} from 'redux';

import pagetitle from './pagetitle_reducer';
import posts from './posts_reducer';

export default combineReducers({
	pagetitle,
	posts
})
