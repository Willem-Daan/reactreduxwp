import { FETCH_POST,
				 FETCH_POSTS,
				 FETCH_POSTS_START,
				 FAILED_POST,
				 FETCH_POST_FAILED } from '../actions';

const initialState = {
	fetched: false,
	failed: false,
	posts: []
}

export default (state = initialState, action) => {
	switch (action.type){
		case FETCH_POSTS_START:
			return {...state, fetched: false, posts: []}
		case FETCH_POSTS:
			return {...state, fetched: true, posts: action.payload};
		case FETCH_POST:
			return {...state, fetched: true, posts: action.payload};
		case FAILED_POST:
			return {...state, failed: true};
		case FETCH_POST_FAILED:
			return {...state, failed: true};
		default:
			return state;
	}
}
