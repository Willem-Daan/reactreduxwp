import { SHOW_TITLE, SHOW_TITLE_START, FAILED_TITLE } from '../actions';

const initialState = {
	fetched: false,
	failed: false,
	pagetitle:'' ,
	desc:''
}

export default (state = initialState, action) => {
	switch (action.type){
		case SHOW_TITLE_START:
			return {...state, fetched: false, pagetitle: '', desc:''}
		case SHOW_TITLE:
			return {...state, fetched: true, pagetitle: action.title, desc: action.desc };
		case FAILED_TITLE:
			return {...state, failed: true};
		default:
			return state;
	}
}
