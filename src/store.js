import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import combinedReducers from './reducers';
import createHistory from 'history/createBrowserHistory';
//import { routerMiddleware } from 'react-router-redux';
import { connectRouter, routerMiddleware } from 'connected-react-router'

const history = createHistory();

// const appliedMiddleware = applyMiddleware(thunk, createLogger(), routerMiddleWare(history));
const appliedMiddleware = applyMiddleware(thunk,  createLogger(),routerMiddleware(history));

export default createStore(
  connectRouter(history)(combinedReducers),
  compose(appliedMiddleware)
)
