//libs
import axios from 'axios';

//action vars SiteTitle
export const SHOW_TITLE = 'SHOW_TITLE';
export const SHOW_TITLE_START = 'SHOW_TITLE_START';
export const FAILED_TITLE = 'FAILED_TITLE';

//action vars Fetchposts
export const FETCH_POSTS = 'FETCH_POSTS';
export const FETCH_POSTS_START = 'FETCH_POSTS_START';
export const FAILED_POST = 'FAILED_POST';

export const FETCH_POST = 'FETCH_POST';
export const FETCH_POST_FAILED = 'FAILED_POST'

//baseurl
const url = 'http://192.168.1.134:8080';


//action Site title
export function fetchSiteTitle() {
  return function (dispatch) {
    dispatch({type: SHOW_TITLE_START});
    axios.get(`${url}/wp-json/`)
        .then(response => {
            dispatch({
                type: SHOW_TITLE,
                title: response.data.name,
                desc: response.data.description
            });
        });
  }
}

//action Posts list
export function fetchPosts(post_type = 'posts') {
    return function (dispatch) {
        dispatch({type: FETCH_POSTS_START});
        axios.get(`${url}/wp-json/wp/v2/${post_type}`)
            .then(response => {
                dispatch({
                    type: FETCH_POSTS,
                    payload: response.data
                });
            });
    }
}


//action SinglePost
export function fetchSinglePost(post_type, postSlug) {
    return function (dispatch) {
        dispatch({type: FETCH_POSTS_START});
        axios.get(`${url}/wp-json/wp/v2/${post_type}?slug=${postSlug}`)
            .then(response => {
                dispatch({
                    type: FETCH_POST,
                    payload: response.data
                });
            })
    }
}
